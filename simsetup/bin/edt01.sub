*Library of components of CEDT PE Laboratory
*$
*----------------------------------------------------------------
* Bondgraph two port devices
*------------------------------------------------------------------

*TFPT
*       pins  1  2  3  4
.SUBCKT tfpt np np0 ns ns0 N=1
BEs1 ns nsmid V=v(np,np0)*{N}
vs1 ns0 nsmid 0
BFp1 np np0 I=i(vs1)*{N}
rsp1 np ns 100e6
rsm1 np0 ns0 100e6
.ENDS

*TFCT
*       pins  1  2  3  4
.SUBCKT tfct np np0 ns ns0 N=1
BEp1 np npmid V=v(ns,ns0)*{N}
vp1 npmid np0 0
BFs1 ns0 ns I=i(vp1)*{N}
rsp1 np ns 100e6
rsm1 np0 ns0 100e6
.ENDS


*GYPT
*       pins  1  2  3  4
.SUBCKT gypt np np0 ns ns0 N=1
vp1 npmid np0 0
vs1 ns0 nsmid 0
BHs1 ns nsmid I=v(np0,np)/{N}
BHp1 np npmid I=v(ns,ns0)/{N}
rsp1 np ns 100e6
rsm1 np0 ns0 100e6
.ENDS

*GYCT
*       pins  1  2  3  4
.SUBCKT gyct np np0 ns ns0 N=1
vp1 npmid np0 0
vs1 ns0 nsmid 0
BGs1 ns nsmid V=i(vp1)*{N}
BGp1 np npmid V=i(vs1)*{N}
rsp1 np ns 100e6
rsm1 np0 ns0 100e6
.ENDS

*GY
*       pins  1  2  3  4
.SUBCKT gy np np0 ns ns0 N=1
rp1 np nrp 1e-10
rs1 ns nrs 1e-10
rpmid nrp nrpmid 1e-10
rsmid nrs nrsmid 1e-10
BGp1 nrpmid npmid V=(-1)*i(vs1)*{N}
BGs1 nrsmid nsmid V=i(vp1)*{N}
vp1 npmid np0 0
vs1 nsmid ns0 0
BHs1 nrs nrsmid I=(-1)*v(nrp,np0)/{N}
BHp1 nrp nrpmid I=v(nrs,ns0)/{N}
*Uncomment and give 1% permeance value to include leakage effects
*Clp nrp np0 100nF
*Cls nrs ns0 100nF
rsp1 nrp nrs 100e6
rsm1 np0 ns0 100e6
.ENDS


*TF
*    pins   1  2   3  4
.SUBCKT tf np np0 ns ns0 N=1
xGYp np np0 n1 n10 gy N={1}
xGYs n1 n10 ns ns0 gy N={N}
.ENDS

*Electro-Magnetic Transformer
*    pins   1  2   3  4
.SUBCKT xfmer np np0 ns ns0 Np=1 Ns=1 C=100uF
xGYp np np0 n1 n10 gy N={Np}
xGYs n2 n10 ns ns0 gy N={Ns}
*                IF Vc  < fsat   THEN {Cper} ELSE  {reducing Cper}
Cper n1 n2 c='v(n1)-v(n2) < {5} ? {C} : {0.99*C*exp(-10*(v(n1)-v(n2)))+0.01*C}'
.ENDS

*FORWARD Xfm
*       pins  1  2  3  4   5  6
.SUBCKT xfmfor np np0 nd nd0 ns ns0 Np=1 Nd=1 Ns=1 C=100uF
xGYp np np0 n1 n10 gy N={Np}
xGYd n1 n30 nd nd0 gy N={Nd}
xGYs n2 n10 ns ns0 gy N={Ns}
*                IF Vc  < fsat   THEN {Cper} ELSE  {reducing Cper}
Cper n30 n2 c='v(n30)-v(n2) < {5} ? {C} : {0.99*C*exp(-10*(v(n30)-v(n2)))+0.01*C}'
.ENDS

*PUSHPULL Xfm
*       pins     1   2   3   4   5  6
.SUBCKT xfmpush np1 npc np2 ns1 nsc ns2 Np=1 Ns=1 C=100uF
xGYp1 np1 npc n1 n10 gy N={Np}
xGYp2 npc np2 n10 n2 gy N={Np}
xGYs1 n3 n30 ns1 nsc gy N={Ns}
xGYs2 n30 n2 nsc ns2 gy N={Ns}
*                IF Vc  < fsat   THEN {Cper} ELSE  {reducing Cper}
Cper n1 n3 c='v(n1)-v(n3) < {5} ? {C} : {0.99*C*exp(-10*(v(n1)-v(n3)))+0.01*C}'
.ENDS

*BRIDGE Xfm
*       pins     1   2   3   4   5
.SUBCKT xfmbdge np1 np2 ns1 nsc ns2 Np=1 Ns=1 C=100uF
xGYp  np1 np2 n1 n10 gy N={Np}
xGYs1 n3 n30 ns1 nsc gy N={Ns}
xGYs2 n30 n10 nsc ns2 gy N={Ns}
*                IF Vc  < fsat   THEN {Cper} ELSE  {reducing Cper}
Cper n1 n3 c='v(n1)-v(n3) < {5} ? {C} : {0.99*C*exp(-10*(v(n1)-v(n3)))+0.01*C}'
.ENDS

*MULTI-OUTPUT FLYBACK
*    pins       1  2   3   4    5    6   7   8  
.SUBCKT xfmfly np np0 ns1 ns10 ns2 ns20 ns3 ns30 Np=1 Ns1=1 Ns2=1 Ns3=1 C=100uF
xGYp np np0 n1 n10 gy N={Np}
xGYs1 n2 n20 ns1 ns10 gy N={Ns1}
xGYs2 n20 n30 ns2 ns20 gy N={Ns2}
xGYs3 n30 n10 ns3 ns30 gy N={Ns3}
*                IF Vc  < fsat   THEN {Cper} ELSE  {reducing Cper}
Cper n1 n2 c='v(n1)-v(n2) < {5} ? {C} : {0.99*C*exp(-10*(v(n1)-v(n2)))+0.01*C}'
.ENDS


*****************************************************************************
* Diodes
*****************************************************************************
.model Def  D()
*--------------------------
*Diode macro model
.SUBCKT diode_power 101 102
DX    101 102   def_power_diode
Rsh   102 103   10000.0
Csh   103 101   0.01uF
.model  def_power_diode  D(RS=0.01, CJO=100pF)
.ENDS
*$
*-------------------------------------------------
.model switch SW( RON=0.001 )
*$
*-------------------------------------------------
*Update: Note ground referenced input
.SUBCKT  power_sw  nSp  nSn  nVcp 
SW      nSp  nDb  nVcp  0  bidir_sw
Dblock  nDb  nSn  d_switch
Dbody   nSn  nSp  d_switch
.model  bidir_sw  SW( RON=0.001 )
.model  d_switch  D()
.ENDS
*$
*------------------------------------------------------------------------------
.SUBCKT PID nerr nVc Ki=1 Kp=1 Kd=0 lsat=-1.0 usat=1.0
aint nerr ni pid_int
.model pid_int int(gain={Ki} out_lower_limit={lsat} out_upper_limit={usat})

adif nerr nd pid_dif
.model pid_dif d_dt(gain={Kd} out_lower_limit={lsat} out_upper_limit={usat})

asigma [nerr ni nd] nVsum pid_sum
.model pid_sum summer(in_gain =[{Kp} 1.0 1.0])

apidlimit nVsum nVc  control_limit
.model control_limit limit(out_lower_limit={lsat} out_upper_limit={usat})
.ENDS
*-----------------------------------------------------------------------------
.SUBCKT PWMtri nPin nPout fs=10000
atri nPin nPtri triout
.model triout triangle(cntl_array=[0 1] freq_array=[{fs} {fs}] out_low = -1.0 out_high = 1.0 duty_cycle = 0.5)
acompare [nPin nPtri] ncout comp_sum
.model comp_sum summer(in_gain=[1.0 -1.0])
alim ncout nPout limit_comp
.model limit_comp limit(gain=1e6 out_lower_limit=0.0 out_upper_limit=1.0 fraction=FALSE)
.ENDS

*-------------------------------------------------
.SUBCKT PWMbridge nPin nArm1T nArm1B nArm2T nArm2B fs=10000
atri1 nPin nPtri1 triout1
.model triout1 triangle(cntl_array=[0 1] freq_array=[{fs} {fs}] out_low = -1.0 out_high = 1.0 duty_cycle = 0.5)
ainv nPin nPinv amp
.model amp gain(gain=-1.0)
acompare1 [nPin nPtri1] ncout1 comp_sum
acompare2 [nPtri1 nPinv] ncout2 comp_sum
.model comp_sum summer(in_gain=[1.0 -1.0])
alim1 ncout1 nArm1T limit_comp
alim2 ncout2 nArm1B limit_comp
alim3 ncout2 nArm2T limit_comp
alim4 ncout1 nArm2B limit_comp
.model limit_comp limit(gain=1e6 out_lower_limit=0.0 out_upper_limit=1.0 fraction=FALSE)
.ENDS
*$
*------------------------------------------------------
.SUBCKT SCR 101 103 T=20ms TDLY=1ms ICGATE=0V
SW      101 102  53 0 SWITCH
VSENSE  102 103  0V
RSNUB   101 104  200
CSNUB   104 103  1uF
VGATE   51  0  PULSE(0 1V {TDLY} 0 0 ({T}/40) {T})
RGATE   51  0  1MEG
EGATE   52  0  TABLE {I(VSENSE)+V(51)} = (0.0,0.0) (0.1,1.0) (1.0,1.0)
RSER    52  53 1
CSER    53  0  1uF  IC={ICGATE}
.MODEL  SWITCH SW( RON=0.01 )
.ENDS
*-----------------------------------------------------------------------
.SUBCKT sum np nm nout
asum [np nm] nout sum_err
.model sum_err summer(in_gain=[1.0 -1.0])
.ENDS
*-------------------------------------------
.SUBCKT clock nclk fc=10000 duty=0.5 phase=0
venable nen 0 1
aclock nen nclkd varclock
.model varclock d_osc(cntl_array = [0 1]
+                      freq_array = [{fc} {fc}]
+                      duty_cycle = {duty} init_phase={phase})
adac [nclkd] [nclk] dac_buff
.model dac_buff dac_bridge(out_low = 0.0 out_high = 1.0)
.ENDS
*-----------------------------------------------------------------------
.SUBCKT SR_FF nset nrst nq
asat1 nset nsetsat schmitff
asat2 nrst nrstsat schmitff
.model schmitff limit(gain=1e6 out_lower_limit=0 out_upper_limit=1.0)
venable nena 0 1
a_adc1 [nsetsat] [nsetd] adc_buff
a_adc2 [nrstsat] [nrstd] adc_buff
a_adc3 [nena] [nend] adc_buff
.model adc_buff adc_bridge(in_low = 0.5 in_high = 0.5)
alatch nsetd nrstd nend "null" "null" nqd nqbar latchsr
.model latchsr d_srlatch
adac [nqd] [nq] dac_buff
.model dac_buff dac_bridge(out_low = 0.0 out_high = 1.0)
.ENDS
*-------------------------------------------

*LIM
*       pins  1  2 3
.SUBCKT lim nvin nvout lgnd lowerlimit=0 upperlimit=1
bvout nvout lgnd v = v(nvin,lgnd)>upperlimit ? upperlimit : v(nvin,lgnd)<lowerlimit? lowerlimit : v(nvin,lgnd)
.ENDS

*-------------------------------------------

*PWMGenerator
*       pins  1  2 3
.SUBCKT pwms nvin nvout lgnd  fs=20000 dmin=0.1 dmax=0.9
Xlim100 nvin ninlimit lgnd lim lowerlimit=dmin upperlimit=dmax
atri lgnd ntriangle triout
.model triout triangle(cntl_array=[0 1] freq_array=[{fs} {fs}] out_low = 0 out_high = 1.0 duty_cycle = 0.5)
bvout nvout lgnd v = v(ninlimit, ntriangle)> 0 ? 1 : -1
.ENDS

