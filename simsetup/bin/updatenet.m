function [param_col,param_col_valstr] = updatenet(netfilename)

readnetfile=['read_',netfilename];
system(['cp ',netfilename,' ',readnetfile]);
fid1=fopen(readnetfile,'r');
fid2=fopen(netfilename,'w+');
fline=fgetl(fid1);
tline=strtrim(fline);
param_col='';param_col_valstr='';
flag=1;line_no=1;cind=1;
while flag==1 
   if ~isempty(tline)
	if strncmpi(tline,'L',1)
		%Inductor netlist line
		bindex=findstr(tline,' ');
		param_str=['i',tline(1:bindex(1)-1)];
		Lline_str=[tline,' IC={',param_str,'}'];
                if isempty(strfind(tolower(tline),' ic'))
		    fprintf(fid2,'%s\r\n',Lline_str);
		    param_col(cind,:)=param_str;
		    param_col_valstr(cind,:)=['i(',tline(1:bindex(1)-1),')'];
		    cind=cind+1;
		else
		    fprintf(fid2,'%s\r\n',tline);
		endif

	else
		fprintf(fid2,'%s\r\n',tline);
	endif
   endif
line_no=line_no+1;
fline=fgetl(fid1);
	if fline==-1
		flag=0;
	else
		tline=strtrim(fline);

	endif
endwhile 
fclose(fid2);
fclose(fid1);
endfunction
