## Script file for plotting the data from raw file   
plot_cnt=0;
multi_flag="";
file_cnt=0;
k=2;
while k==2,
clc;
disp('SELECT X-axis and Y-axis');
disp(' ');
for i = 1:length(col)
disp([num2str(i),". ",col{i,1}]);
endfor
disp(' ');
disp('TO QUIT: enter x-axis = 0');
disp(' ');
clear plotxstr;
clear plotystr;

xax=input(['ENTER x-axis number (press ENTER for ''',col{1,1},''' as default): '],'s');
    if isempty(xax)
	xax=col{1,1}; ##default
    endif
    if (strcmp(xax,'0')),
	if strcmpi(multi_flag,"M")  ## save subplots figure window on exit
	  for cnt=1:plot_cnt
	    subplot(2,2,cnt);eval(mplot_str{cnt,1});xlabel(["(",xalpha(cnt),") ",mxax{cnt,1}]);grid;
	  endfor
	  file_cnt=file_cnt+1;
	  if (file_cnt==1)		      
      		prt_str=['print -d',imgstr,' ',prt_file_name,'.',imgstr];
	  else
		prt_str=['print -d',imgstr,' ',prt_file_name,'n',num2str(file_cnt),'.',imgstr];
	  endif
	  eval(prt_str);
	endif
     clc;
     k=20;
    elseif (isempty(findstr(xax,","))) ## there should be only one variable
     ##find x-axis data****************************************
     xax=strtrim(xax);
	   plotxstr=xax;
	   ij=1;
	   for j = 1:length(col)
	   	if ~isempty(findstr(xax,col{j,1}))
	   		ylen(ij)=length(col{j,1});colno(ij)=j;ij=ij+1;
		   endif
	   endfor
	   [ss,ii]=sort(ylen);
	   for j = 1:length(ylen)
	      jj = length(ylen)+1-j;
		   plotxstr=strrep(plotxstr,col{colno(ii(jj)),1},["data(:,",num2str(colno(ii(jj))),")"]); 
	   endfor
	   plotxstr=strrep(plotxstr,"+",".+");
	   plotxstr=strrep(plotxstr,"-",".-");
	   plotxstr=strrep(plotxstr,"*",".*");
	   plotxstr=strrep(plotxstr,"/","./");
	   plotxstr=strrep(plotxstr,"^",".^");
      ##find y-axis data*****************************************
     yax=input('ENTER y-axis variable ...comma separate for multiple variables: ','s');
	yax=strtrim(yax);
	yind=[0,findstr(yax,","),length(yax)+1];
	for i = 2:length(yind)
	   plotystr{i-1,1}=yax(yind(i-1)+1:yind(i)-1);
	   ij=1;
	   for j = 1:length(col)
	   	if ~isempty(findstr(yax(yind(i-1)+1:yind(i)-1),col{j,1}))
	   	   ylen(ij)=length(col{j,1});colno(ij)=j;ij=ij+1;
		   endif
	   endfor
	   [ss,ii]=sort(ylen);
	   for j = 1:length(ylen)
	      jj = length(ylen)+1-j;
	    	plotystr{i-1,1}=strrep(plotystr{i-1,1},col{colno(ii(jj)),1},["data(:,",num2str(colno(ii(jj))),")"]); 
	   endfor
	   plotystr{i-1,1}=strrep(plotystr{i-1,1},"+",".+");
	   plotystr{i-1,1}=strrep(plotystr{i-1,1},"-",".-");
	   plotystr{i-1,1}=strrep(plotystr{i-1,1},"*",".*");
	   plotystr{i-1,1}=strrep(plotystr{i-1,1},"/","./");
	   plotystr{i-1,1}=strrep(plotystr{i-1,1},"^",".^");
	   pcolor=(i-1)-(ceil((i-1)/6)-1)*6; ## set plot colour
	   plotystr{i-1,1}=[plotystr{i-1,1},",""",num2str(pcolor),";",(yax(yind(i-1)+1:yind(i)-1)),";"""];
	endfor

     ##PLOT***************************************
     evalplotstr="plot(";
     for i = 1:length(plotystr)
	if i==length(plotystr)
           evalplotstr= sprintf("%s",[evalplotstr,plotxstr,",",plotystr{i,1},")"]);
	else
	   evalplotstr= sprintf("%s",[evalplotstr,plotxstr,",",plotystr{i,1},","]);
	endif
     endfor
     eval(evalplotstr);pause(1);
     ax=axis;
     xmin=ax(1);xmax=ax(2);ymin=ax(3);ymax=ax(4);
	##*************************************
	##********zooming********
	clc;
	zflag=menu('Continue or Zoom', 'Continue','Zoom figure');
	if (zflag==2)
	  ax=input('Enter min and max axis limits in format [xmin xmax ymin ymax] = ');
	  xmin=ax(1);xmax=ax(2);ymin=ax(3);ymax=ax(4);
	endif
	while (zflag==2)|(zflag==3)
		axis([xmin,xmax,ymin,ymax]);replot;
		clc;
		zflag=menu('Continue, Zoom or Restore','Continue', 'Zoom figure','Restore figure');
		if (zflag==2)
		  ax=input('Enter min and max axis limits in format [xmin xmax ymin ymax] = ');
		  xmin=ax(1);xmax=ax(2);ymin=ax(3);ymax=ax(4);
		elseif (zflag==3)
     		  eval(evalplotstr);pause(1);
		  ax=axis;
	          xmin=ax(1);xmax=ax(2);ymin=ax(3);ymax=ax(4);
		else
		endif
	endwhile
     ##*******************************************
     ##SAVE PLOT as SVG figures
	 if ~strcmpi(multi_flag,"M")
	   save_flag=input('Do you want to save the plots for documentation [Y/N] : ','s');
	 endif
	 if strcmpi(save_flag,"Y")
	 clc;
	   if ~strcmpi(multi_flag,"M")
	     disp("The plots will be saved as .svg or .pdf. For .svg files, use inkscape to edit and save as .pdf");
	     disp("For multiple plots, atmost 4 plots will be save per file.");
	     disp(" ");
	     multi_flag=input('Do you want a Single plot in a page or Multiple plots per page [S/M] : ','s');
	     prt_file_name=input('Enter the filename to save into - (enter just name without extension) : ','s');
		imgtype=menu('SVG or PDF output?','SVG', 'PDF');
		if (imgtype==1)
		   imgstr='svg';
		elseif (imgtype==2)
	     	   imgstr='pdf';
		else
		endif
	   endif
		xalpha="abcd";
		if strcmpi(multi_flag,"S")
		   xlabel(xax);grid;
		   prt_str=['print -d',imgstr,' ',prt_file_name,'.',imgstr];
		   eval(prt_str);
		elseif strcmpi(multi_flag,"M")
		    plot_cnt=plot_cnt+1;
		    mplot_str{plot_cnt,1}=[evalplotstr,';axis([',num2str(xmin),',',num2str(xmax),',',num2str(ymin),',',num2str(ymax),']);replot;'];
		    mxax{plot_cnt,1}=xax;
		   if (plot_cnt==4)
		      for cnt=1:plot_cnt
			subplot(2,2,cnt);eval(mplot_str{cnt,1});xlabel(["(",xalpha(cnt),") ",mxax{cnt,1}]);grid;
		      endfor
		      file_cnt=file_cnt+1;
			if (file_cnt==1)		      
		      		prt_str=['print -d',imgstr,' ',prt_file_name,'.',imgstr];
			else
				prt_str=['print -d',imgstr,' ',prt_file_name,'n',num2str(file_cnt),'.',imgstr];
			endif
		      eval(prt_str);
		      plot_cnt=0;
		      clear mplot_str;
		      clear mxax;
		      clf;
		   else
		   endif	
		else
		endif
	 endif
     ##*******************************************
    else
     disp('Invalid axes variable strings');
    endif
endwhile



