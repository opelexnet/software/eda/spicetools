SIMULATION with gEDA-ngSPICE-octave 

Simulating multi-energy domain systems and non-linear systems can be a real challenge. We have tried to achieve this using three open source packages viz.
    (i) gEDA - Electronic Design Automation package
    (ii) ngSPICE - SPICE simulation package
    (iii) octave - a package for scripting and programming

The three packages mentioned above are essential for simulation. If one needs to consider the outputs of the above integrated simulation environment for documentation purposes, then it may be desirable to have the following three packages too:
    (i) inkscape - vector drawing program
    (ii) xmlstarlet - xml editing program that can be used to edit svg files
    (iii) pdfjam - pdf page management program

We have tried installing and implementing the following in Fedora 13 and 14. We have not tried for other linux distributions, however by using appropriate equivalent commands it should be possible in other distributions as well. 

INSTALL THE PROGRAMS (use appropriate commands based on distribution)
yum install geda\* pcb gerbv
yum install ngspice
yum install octave
yum install inkscape
yum install xmlstarlet
yum install pdfjam

INSTALL ESE SCRIPTS (ESE stands for Electronic Systems Engineering)
Download "simsetup" folder from 10.114.14.58 (power lab git hub)
Device symbols are stored in "abond", "ablock" and "acomps" folder
     abond - this folder contains bondgraph related components (power blocks and symbols)
     ablock - this folder contains block diagram components (signal or info blocks)
     acomp - this folder contains circuit based components
Device models and subckt libraries are stored in edt01.sub
On your system, as su, copy abond, ablock and acomps to /usr/share/gEDA/sym/
                       chmod -v -R 755 a*  (make them read and executable)
                       cd /usr/share/gEDA/gafrc.d/
                       gedit geda-clib.scm
	                  include abond, ablock and acomps under Generic symbols as follows
                              ; Generic symbols
                              ("abond" "Bond graph")
                              ("ablock" "Block diagram")
                              ("acomps" "Ckt components")
copy bin folder to /home/user/bin/
/home/user/bin/ should contain 
	edt01.sub
	ngsim
	ngplot.m
	spice_readfile.m
	slidescape
	makeslides
        updatenet.m
        gsch2net
        gsymUpdate
        gtxt2sym

