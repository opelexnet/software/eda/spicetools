%-----------------------------------------------CUK CONVERTER-------------------------%
%Design Spec: Vo = 15V, Io = 1.5Amp, Fs = 10KHz., Vin = 10V------------%
%Selection of Components: R = 10 ohms, L1 = 10mH, L2 = 10mH,
%C1 =100uF, C2 = 10uF, Sampling Frequency = 5Khz

clear all;
clc
disp(['The values of the circuit parameters']);
d = 0.6;
L1 = 10e-3;
L2 = 10e-3;
C1 = 100e-6;
C2 = 10e-6;
R = 10;
Vg =10;
Ts=1/5000;

%small signal model
Ac = [0 0 -(1-d)/L1 0;0 0 -d/L2 -1/L2;(1-d)/C1 d/C1 0 0;0 1/C2 0 -1/(R*C2)];
Bc1 = [1/L1; 0; 0; 0];
X = -(inv(Ac))*Bc1*Vg; %steady state values
Bc2 = [X(3)/L1; -X(3)/L2; (-X(1)+X(2))/C1; 0];
Bc=[Bc1,Bc2];
Cc = [0 0 0 1];
Dc1=0;Dc2=0;
Dc = [Dc1 Dc2];
ulabels=['Vg d'];
ylabels=['Vo'];
xlabels=['iL1 iL2 Vc1 Vo'];


%-----Continious time transfer function of plant is given by---------------%
disp('Transfer function in s-domain (Vo(s)/d(s))')
TF2 = zpk(tf(ss(Ac,Bc,Cc,Dc)))

%Conversion from continuous to discrete domain state space form
%[numd,dend]=c2dm(num,den,Ts,'zoh');               %for system in transfer function form
[A,B,C,D]=c2dm(Ac,Bc,Cc,Dc,Ts,'zoh'); %for system in state space form
[ng,dg]=ss2tf(A,B,C,D,2); %numerator and denominator polynomical of plant
%with respect to input 2 i.e. d input
%[A,B,C,D]=tf2ss(numd,dend); %transfer function to state space form

disp('Transfer function in z-domain Vo(z)/d(z)')
TFz = zpk(tf(ng,dg,1))
printsys(A,B,C,D,ulabels,ylabels,xlabels)

