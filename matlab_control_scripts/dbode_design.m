%This is a script file to demonstrate the time response plots and the
%use of bode plots to determine the loop gain

%Run the plant model file - like the dc_model.m that is
%provided as an example
dc_model;%model of dc motor

%Transfer function Gc of the controller in
K=6; %controller gain
ngc=K*[1, -0.9];
dgc=[1,-1];%[1,-1];

%Transfer function H
nh=[1];
dh=[1];

%loop Transfer function, GH
nl=conv(conv(ng,ngc),nh);
dl=conv(conv(dg,dgc),dh);

%bode plots of the open loop plant
w=logspace(-1,2);
[magp,phasep] = dbode(ng,dg,Ts,w);
figure;
subplot(2,1,1), semilogx(w,20*log10(magp)), grid
subplot(2,1,2), semilogx(w,phasep), grid
    
%Gain and phase margins of open loop plant
[Gm,Pm,Wcg,Wcp] = margin(magp,phasep,w);
disp(['Gain margin in dB = ', num2str(20*log10(Gm)), ' at frequency ', num2str(Wcg)]);
disp(['Phase margin in degree = ', num2str(Pm), ' at frequency ', num2str(Wcp)]);

pause
%bode plot of the controller
[mag,phase] = dbode(ngc,dgc,Ts,w);
figure;
subplot(2,1,1), semilogx(w,20*log10(mag)), grid
subplot(2,1,2), semilogx(w,phase), grid

pause
%bode plot of controller + system
[magc,phasec] = dbode(nl,dl,Ts,w);
figure;
subplot(2,1,1), semilogx(w,20*log10(magp),w,20*log10(magc)), grid
subplot(2,1,2), semilogx(w,phasep,w,phasec), grid

%Gain and phase margins of compensated plant
[Gm,Pm,Wcg,Wcp] = margin(magc,phasec,w);
disp(['Gain margin in dB = ', num2str(20*log10(Gm)), ' at frequency ', num2str(Wcg)]);
disp(['Phase margin in degree = ', num2str(Pm), ' at frequency ', num2str(Wcp)]);


%closed loop transfer function = G/(1+GH)
[nc,dc]=feedback(conv(ng,ngc),conv(dg,dgc),nh,dh);

pause
figure;
dstep(nc,dc,100); %step response of closed loop transfer function for K=1