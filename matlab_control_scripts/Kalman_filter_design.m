%Design of Kalman filter/estimator

%Run the plant model file - example dc_model.m
cuk_model

clc
% Design of Kalman Estimator
Sys = ss(A,B, C, D);
Qn=[0.1 ]*eye(length(A)) %......  Plant Noise covariance
[row_C,col_C]=size(C);
Rn=[0.1]*eye(row_C)  %.......Measurement Noise covariance
n=1000;
p= [0.1 0 0 0; 0 0.1 0 0; 0 0  0.1 0; 0 0 0 0.1]

for i=1:n
    p= A*p*A'+ Qn;      %time update
    p= p-p*C'*inv(C*p*C'+Rn)*C*p;  %measurement update
    Gk=p*C'*inv(Rn); % kalman Estimator
end
p
Gk