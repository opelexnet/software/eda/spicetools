%This is a script file to demonstrate the time response plots and the
%use of root locus method to determine the controller gain

%Transfer function of the plant or system
ng=[1];
dg=[1 0.5];
%Transfer function Gc of the controller in
%terms of pole and zero locations
ngc=[1];%[1,-3.5];
dgc=[1,-1];%[1,1];

%Transfer function H
nh=[1];
dh=[1];

%loop Transfer function, GH
nl=conv(conv(ng,ngc),nh);
dl=conv(conv(dg,dgc),dh);

%closed loop transfer function = G/(1+GH)
[nc,dc]=feedback(conv(ng,ngc),conv(dg,dgc),nh,dh);

dstep(nc,dc,20); %step response of closed loop transfer function for K=1

flag=1;
while flag~=0
   %root locus for loop transfer function
zgrid('new');
rlocus(nl,dl);
[k,p]=rlocfind(nl,dl)

[nc,dc]=feedback(conv(ng,ngc)*k,conv(dg,dgc),nh,dh); %Closed loop tr. fn. new value of k
clf;
dstep(nc,dc,100); %step response with desired gain k
flag=input('Enter 0 to quit or 1 to continue [0/1] = ');
end

