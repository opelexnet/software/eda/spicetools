%State space design methods

%state feed back controller design
flag=1;
while flag==1
    Pc=input('Enter the row vector of desired closed loop poles [..] = ');
    K = acker(A,B,Pc);
    [y,x,n]=dinitial(A-B*K,zeros(size(B)),C,D,ones(length(A),1));%closed loop response
    disp(['Gain, K = ', num2str(K)]);
    subplot(1,2,1), stairs([1:n],x);xlabel('no. of samples, n');ylabel('state');
    subplot(1,2,2), stairs([1:n],-K*x');xlabel('no. of samples, n');ylabel('input');
    flag=input('Do you want to tune the controller? Press 1 to tune, 0 to exit tuning [1/0] = ');
    clc
end %of while

%Estimator design (Prediction estimator)
flag=0;
flag=input('Do you want to design prediction estimator gain - type 1 for yes and 0 for no = ');
while flag==1
    clc
    Pe=input('Enter the row vector of desired closed loop estimator poles [..] = ');
    G = (acker(A',C',Pe))';
    [y,e,n]=dinitial(A-G*C,zeros(size(B)),C,D,ones(length(A),1));%closed loop response
    disp(['Gain, G'' = ', num2str(G')]);
    stairs([1:n],e);xlabel('no. of samples, n');ylabel('state error');
    flag=input('Do you want to tune the estimator? Press 1 to tune, 0 to exit tuning [1/0] = ');
end %of while

%Estimator design (Current estimator)
flag=0;
flag=input('Do you want to design current estimator gain - type 1 for yes and 0 for no = ');
while flag==1
    clc
    Pe=input('Enter the row vector of desired closed loop estimator poles [..] = ');
    G = (acker(A',A'*C',Pe))';
    [y,e,n]=dinitial(A-G*C*A,zeros(size(B)),C,D,ones(length(A),1));%closed loop response
    disp(['Gain, G'' = ', num2str(G')]);
    stairs([1:n],e);xlabel('no. of samples, n');ylabel('state error');
    flag=input('Do you want to tune the estimator? Press 1 to tune, 0 to exit tuning [1/0] = ');
end %of while

%Estimator design (Reduced order estimator)
flag=0;
flag=input('Do you want to design reduced order prediction estimator gain - type 1 for yes and 0 for no = ');
if flag==1,
    ei=input('Enter the number of states in the state vector starting from top = ');
    Aee=A(1:ei,1:ei);
    Ame=A(ei+1:length(A),1:ei);
end
while flag==1
    clc
    Pe=input('Enter the row vector of desired closed loop estimator poles [..] = ');
    G = (acker(Aee',Ame',Pe))';
    [y,e,n]=dinitial(Aee-G*Ame,zeros(size(B(1:ei,:))),Ame,D,ones(length(Aee),1));%closed loop response
    disp(['Gain, G'' = ', num2str(G')]);
    stairs([1:n],e);xlabel('no. of samples, n');ylabel('state error');
    flag=input('Do you want to tune the estimator? Press 1 to tune, 0 to exit tuning [1/0] = ');
end %of while


%Design of Nx and Nu matrices for tracker design
clc
disp('DESIGN PARAMETERS FOR THE CONTROLLER/ESTIMATOR SYSTEM');
[cr,cc]=size(C);
[br,bc]=size(B);
NN=inv([A-eye(size(A)),B;C,zeros(cr,bc)])*[zeros(length(A),cr);eye(cr)];
[nr,nc]=size(NN);
Nx=NN(1:length(A),:)
Nu=NN(length(A)+1:nr,:)
disp(['Gain, K = ', num2str(K)]);
disp(['Gain, G'' = ', num2str(G')]);

%....Design of optimal controller-------%`
Q = Cd'*Cd
R = 1
P = dare(A,B,Q,R)
Ko = inv(R + B'*P*B)*B'*P*A
dinitial((A-(B*Ko)),zeros(size(B)),C,D,ones(length(A)));


% Design of Kalman Estimator

Sys = ss(Ad,Fd, Cd, Dd);
Qn=[0.1 ]*eye(length(Ad)) %......  Plant Noise covariance
[row_C,col_C]=size(Cd);
Rn=[0.1]*eye(row_C)  %.......Measurement Noise covariance
n=1000;
p= [0.1 0 0 0; 0 0.1 0 0; 0 0  0.1 0; 0 0 0 0.1]

for i=1:n
    p= Ad*p*Ad'+ Qn;      %time update
    p= p-p*Cd'*inv(Cd*p*Cd'+Rn)*Cd*p;  %measurement update
    Gk=p*Cd'*inv(Rn); % kalman Estimator
end
p
Gk

%... Design of LMS Filter
u = 0.05            % ... the value of mu is obtained form simulink model
                           % .. for minimising the error. 
Glms = 2*u*Cd'  

