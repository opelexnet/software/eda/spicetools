%This is a script file to demonstrate the time response plots and the
%use of root locus method to determine the controller

%Run the system model file - like the dc_model.m that is
%provided as an example.
dc_model;%dc motor model file

%Transfer function Gc of the controller in
%terms of pole and zero locations
ngc=conv([1,-0.95],[1,-0.9]);%[1 -0.55 0.025];%[1,-3.5];
dgc=conv([1,-1],[1,0]);%[1,-0.8 -0.2];%[1,1];

%Transfer function H
nh=[1];
dh=[1];

%loop Transfer function, GH
nl=conv(conv(ng,ngc),nh);
dl=conv(conv(dg,dgc),dh);

%closed loop transfer function = G/(1+GH)
[nc,dc]=feedback(conv(ng,ngc),conv(dg,dgc),nh,dh);

dstep(nc,dc,20); %step response of closed loop transfer function for K=1
pause
clf;

flag=1;
while flag~=0
   %root locus for loop transfer function
   zgrid('new');
   rlocus(nl,dl)
[k,p]=rlocfind(nl,dl)

[nc,dc]=feedback(conv(ng,ngc)*k,conv(dg,dgc),nh,dh); %Closed loop tr. fn. new value of k
clf;
dstep(nc,dc,100); %step response with desired gain k
flag=input('Enter 0 to quit or 1 to continue [0/1] = ');
end

