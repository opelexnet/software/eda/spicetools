%-----------------------------------------------DC motor-------------------------%
clear all;
clc
disp(['The values of the plant parameters']);
Ra = 1; %2;
La = 0.5; %H
Kt = 0.01; %0.1; %Nm/A
B = 2.7e-9; %0.2; %Nms
J = 1e-6; %0.02; %kgm2/s2
Ts = 1e-3; %0.02; %s

%small signal model
Ac = [-Ra/La -Kt/La; Kt/J -B/J];
Bc = [1/La 0;0 -1/J];
Cc = [0 1];
Dc = [0 0];
ulabels=['v TL'];
ylabels=['w'];
xlabels=['ia w'];


%-----Continious time transfer function of plant is given by---------------%
disp('Transfer function in s-domain')
TF2 = zpk(tf(ss(Ac,Bc,Cc,Dc)))

%Conversion from continuous to discrete domain state space form
%[numd,dend]=c2dm(num,den,Ts,'zoh');               %for system in transfer function form
[A,B,C,D]=c2dm(Ac,Bc,Cc,Dc,Ts,'zoh'); %for system in state space form
[ng,dg]=ss2tf(A,B,C,D,1); %numerator and denominator polynomical of plant
%[A,B,C,D]=tf2ss(numd,dend); %transfer function to state space form
%
%TL input is considered as a disturbance input and v input is considered as
%the control input. Therefore, for this system there is only one control
%input. Thus,
B=B(:,1);
D=D(:,1);

disp('Transfer function in z-domain')
TFz = zpk(tf(ng,dg,1))
printsys(A,B,C,D,ulabels,ylabels,xlabels)

