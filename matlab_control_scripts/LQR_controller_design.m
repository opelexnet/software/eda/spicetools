%Design of LQR controller

%Run the plant model file - example dc_model.m
cuk_model

clc
%....Design of optimal controller-------%`
Q = C'*C
[br,bc]=size(B);
R=eye(bc);
P = dare(A,B,Q,R)
Ko = inv(R + B'*P*B)*B'*P*A
dinitial((A-(B*Ko)),zeros(size(B)),C,D,ones(1,length(A)));