%----Non-isolated boost converter-------------------------%
clear all;
clc
disp(['The values of the plant parameters']);
Vg = 15; %V
D = 0.4; %duty ratio
L = 2e-3; %H
C = 10e-6; %F
R = 100; %ohms
Ts = 50e-6; %s 20KHz switching

%steady state model
As = [0 -(1-D)/L; (1-D)/C -1/R/C];
Bs = [1/L 0 0;0 -1/C 0];
Cs = [0 1;1 0];
Ds = [0 0 0;0 0 0];

Vo = -Cs(1,:)*inv(As)*Bs(:,1)*Vg
Ig = -Cs(2,:)*inv(As)*Bs(:,1)*Vg
%small signal model
a = [0 -(1-D)/L; (1-D)/C -1/R/C];
b = [1/L 0 Vo/L;0 -1/C -Ig/C];
c = [0 1];
d = [0 0 0];
ulabels=['vg iz d'];
ylabels=['vo ig'];
xlabels=['il vc'];
printsys(As,Bs,Cs,Ds,ulabels,ylabels,xlabels)
printsys(a,b,c,d,ulabels,ylabels,xlabels)
disp('Transfer function in s-domain')
TF2 = zpk(tf(ss(a,b(:,3),c,[0])))
%rlocus(TF2)

%-----Continious time transfer function of plant is given by---------------%
disp('Transfer function in s-domain')
TF2 = zpk(tf(ss(Ac,Bc,Cc,Dc)))

%Conversion from continuous to discrete domain state space form
%[numd,dend]=c2dm(num,den,Ts,'zoh');               %for system in transfer function form
[A,B,C,D]=c2dm(Ac,Bc,Cc,Dc,Ts,'zoh'); %for system in state space form
[ng,dg]=ss2tf(A,B,C,D,1); %numerator and denominator polynomical of plant
%[A,B,C,D]=tf2ss(numd,dend); %transfer function to state space form
%
%TL input is considered as a disturbance input and v input is considered as
%the control input. Therefore, for this system there is only one control
%input. Thus,
B=B(:,1);
D=D(:,1);

disp('Transfer function in z-domain')
TFz = zpk(tf(ng,dg,1))
printsys(A,B,C,D,ulabels,ylabels,xlabels)

