%Design of prediction estimator gain, G 

%Run the plant model file - example dc_model.m
cuk_model

%Estimator design (Prediction estimator)
flag=1;
while flag==1
    clc
    Pe=input('Enter the row vector of desired closed loop estimator poles [..] = ');
    G = (place(A',C',Pe))';
    [y,e,n]=dinitial(A-G*C,zeros(size(B)),C,D,ones(length(A),1));%closed loop response
    disp(['Gain, G'' = ', num2str(G')]);
    stairs([1:n],e);xlabel('no. of samples, n');ylabel('state error');
    flag=input('Do you want to tune the estimator? Press 1 to tune, 0 to exit tuning [1/0] = ');
end %of while

