%Design of controller gain, K by full state feedback method

%Run the plant model file - example dc_model.m
cuk_model

%state feed back controller design
flag=1;
while flag==1
    Pc=input('Enter the row vector of desired closed loop poles [..] = ');
    K = place(A,B,Pc);
    [y,x,n]=dinitial(A-B*K,zeros(size(B)),C,D,ones(length(A),1));%closed loop response
    disp(['Gain, K = ']);K
    subplot(1,2,1), stairs([1:n],x);xlabel('no. of samples, n');ylabel('state');
    subplot(1,2,2), stairs([1:n],(-K*x')');xlabel('no. of samples, n');ylabel('input');
    flag=input('Do you want to tune the controller? Press 1 to tune, 0 to exit tuning [1/0] = ');
    clc
end %of while
