%Design of reduced order estimator gain, G

%Run the plant model file - example dc_model.m
cuk_model

%Estimator design (Reduced order estimator)
flag=1;
if flag==1,
    ei=input('Enter the number of states in the state vector that needs to be estimated, starting from top = ');
    Aee=A(1:ei,1:ei);
    Ame=A(ei+1:length(A),1:ei);
end
while flag==1
    clc
    Pe=input('Enter the row vector of desired closed loop estimator poles [..] = ');
    G = (acker(Aee',Ame',Pe))';
    [y,e,n]=dinitial(Aee-G*Ame,zeros(size(B(1:ei,:))),Ame,D,ones(length(Aee),1));%closed loop response
    disp(['Gain, G'' = ', num2str(G')]);
    stairs([1:n],e);xlabel('no. of samples, n');ylabel('state error');
    flag=input('Do you want to tune the estimator? Press 1 to tune, 0 to exit tuning [1/0] = ');
end %of while
