%Design of Nx and Nu matrices for tracker design

%Run the plant model file - example dc_model.m
cuk_model

clc
disp('DESIGN PARAMETERS FOR THE CONTROLLER/ESTIMATOR SYSTEM');
[cr,cc]=size(C);
[br,bc]=size(B);
if cr~=bc
    disp(['There are ',num2str(cr),' outputs to be controlled']);
    disp(['Choose ',num2str(cr),' out of ',num2str(bc),' inputs.']);
    in_b=input('Enter the control input numbers [..] = ');
    Bin=zeros(br,length(in_b));
    for i=1:length(in_b)
        Bin=Bin(:,i) + B(:,in_b(i));
    end
else
    Bin=B;
end
[br,bc]=size(Bin);
NN=inv([A-eye(size(A)),Bin;C,zeros(cr,bc)])*[zeros(length(A),cr);eye(cr)];
[nr,nc]=size(NN);
Nx=NN(1:length(A),:)
Nu=NN(length(A)+1:nr,:)

